# spotify-sandbox

just messing around with the spotify API - UI to tweak some parameters & get song recommendations back\
![demo gif](output.gif)

## project setup

- [ ] [create a spotify developer account](https://developer.spotify.com/) and a project using an existing spotify account
- [ ] grab the client ID from [project dashboard](https://developer.spotify.com/) (dashboard -> settings -> basic info -> client ID) and replace INSERT_CLIENT_ID in api/spotify.js
- [ ] set the client ID to the port you're running on 1) in spotify [project dashboard](https://developer.spotify.com/) (dashboard -> settings -> basic info -> edit -> redirect URI) and 2) INSERT_REDIRECT_URI in api/spotify.js
- npm install & npm run serve