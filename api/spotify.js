import "dotenv/config";

async function getGenreSeeds() {
  try {
    let response = await fetch(`${process.env.VUE_APP_BASE_URL}/recommendations/available-genre-seeds`,
      {
        headers: {
          'Authorization': `Bearer  ${localStorage.getItem('access_token')}`,
          'Content-Type': 'application/json'
        }
      });
    response = await response.json();
    return response.genres;
  }
  catch (err) {
    throw new Error(`Could not get Spotify available genres`);
  }
}

export async function getSongRecommendations(requestString) {
  try {
    let response = await fetch(`${process.env.VUE_APP_BASE_URL}/recommendations?${requestString}`, {
      headers: {
        'Authorization': `Bearer  ${localStorage.getItem('access_token')}`,
        'Content-Type': 'application/json'
      }
    });
    response = await response.json();
    return response;
  }
  catch (err) {
    throw new Error(`Could not get song recommendations.`);
  }
}

export async function getAudioAnalyis(songId) {
  try {
    let response = await fetch(`${process.env.VUE_APP_BASE_URL}/audio-analysis/${songId}`, {
      headers: {
        'Authorization': `Bearer  ${localStorage.getItem('access_token')}`,
        'Content-Type': 'application/json'
      }
    });
    return response;
  }
  catch (err) {
    throw new Error(`Could not get audio analysis.`);
  }
}

/**
 * authorization related code
 */
async function getUserData() {
  try {
    let response = await fetch(`${process.env.VUE_APP_BASE_URL}/me/`,
      {
        headers: {
          'Authorization': `Bearer  ${localStorage.getItem('access_token')}`,
          'Content-Type': 'application/json'
        }
      });
    response = await response.json();
    localStorage.setItem('login_state_string', response.display_name + ", " + response.email);
    return response;
  }
  catch (err) {
    throw new Error(`Could not get user data`);
  }
}

export function generateRandomString(length) {
  let text = '';
  const possible =
    'ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789';

  for (let i = 0; i < length; i++) {
    text += possible.charAt(Math.floor(Math.random() * possible.length));
  }
  return text;
}

async function generateCodeChallenge(codeVerifier) {
  const digest = await crypto.subtle.digest(
    'SHA-256',
    new TextEncoder().encode(codeVerifier),
  );

  return btoa(String.fromCharCode(...new Uint8Array(digest)))
    .replace(/=/g, '')
    .replace(/\+/g, '-')
    .replace(/\//g, '_');
}

export function generateUrlWithSearchParams(url, params) {
  const urlObject = new URL(url);
  urlObject.search = new URLSearchParams(params).toString();

  return urlObject.toString();
}

export function redirectToSpotifyAuthorizeEndpoint() {
  const codeVerifier = generateRandomString(64);

  generateCodeChallenge(codeVerifier).then((code_challenge) => {
    window.localStorage.setItem('code_verifier', codeVerifier);
    window.location = generateUrlWithSearchParams(
      'https://accounts.spotify.com/authorize',
      {
        response_type: 'code',
        client_id,
        scope: 'user-read-private user-read-email',
        code_challenge_method: 'S256',
        code_challenge,
        redirect_uri,
      },
    );
  });
}

export function exchangeToken(code) {
  const code_verifier = localStorage.getItem('code_verifier');

  fetch('https://accounts.spotify.com/api/token', {
    method: 'POST',
    headers: {
      'Content-Type': 'application/x-www-form-urlencoded;charset=UTF-8',
    },
    body: new URLSearchParams({
      client_id,
      grant_type: 'authorization_code',
      code,
      redirect_uri,
      code_verifier,
    }),
  })
    .then(addThrowErrorToFetch)
    .then((data) => {
      processTokenResponse(data);

      // clear search query params in the url
      window.history.replaceState({}, document.title, '/');
    })
    .catch(handleError);
}

export function handleError(error) {
  console.error(error);
}

export async function addThrowErrorToFetch(response) {
  if (response.ok) {
    return response.json();
  } else {
    throw { response, error: await response.json() };
  }
}

export function logout() {
  localStorage.clear();
  window.location.reload();
  localStorage.setItem('login_state_string', "Not logged in");
}

async function processTokenResponse(data) {
  let access_token = localStorage.getItem('access_token') || null;
  let refresh_token = localStorage.getItem('refresh_token') || null;
  let expires_at = localStorage.getItem('expires_at') || null;
  access_token = data.access_token;
  refresh_token = data.refresh_token;

  const t = new Date();
  expires_at = t.setSeconds(t.getSeconds() + data.expires_in);

  localStorage.setItem('access_token', access_token);
  localStorage.setItem('refresh_token', refresh_token);
  localStorage.setItem('expires_at', expires_at);

  const response = await getUserData();
  localStorage.setItem('login_state_string', `${response.display_name} (${response.email})`);
  window.dispatchEvent(new CustomEvent('user-logged-in', {
    detail: `${response.display_name} (${response.email})`
  }));
  const genres = await getGenreSeeds();
  localStorage.setItem("spotify_genres", genres);
  window.dispatchEvent(new CustomEvent('genres-set', {
    detail: genres
  }));
}

const client_id = 'INSERT_CLIENT_ID';
const redirect_uri = 'INSERT_REDIRECT_URI';

const args = new URLSearchParams(window.location.search);
const code = args.get('code');

if (code) {
  exchangeToken(code);
} 